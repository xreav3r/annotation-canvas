const heatmaps = {
  jet: [
    [0, 0, 127],
    [0, 0, 127],
    [0, 0, 133],
    [0, 0, 134],
    [0, 0, 143],
    [0, 0, 143],
    [0, 0, 151],
    [1, 0, 152],
    [0, 0, 159],
    [0, 0, 160],
    [0, 0, 167],
    [0, 0, 167],
    [0, 0, 176],
    [0, 0, 177],
    [0, 0, 183],
    [0, 0, 183],
    [0, 0, 191],
    [0, 0, 191],
    [0, 0, 197],
    [0, 0, 197],
    [0, 0, 207],
    [0, 0, 207],
    [1, 0, 215],
    [1, 0, 215],
    [0, 0, 222],
    [0, 0, 223],
    [0, 0, 230],
    [0, 0, 231],
    [0, 0, 240],
    [0, 0, 241],
    [0, 0, 246],
    [1, 0, 247],
    [0, 2, 252],
    [1, 4, 254],
    [0, 9, 252],
    [2, 12, 255],
    [0, 18, 252],
    [0, 20, 254],
    [0, 25, 254],
    [1, 27, 255],
    [0, 33, 252],
    [0, 36, 255],
    [0, 41, 254],
    [0, 42, 255],
    [0, 50, 252],
    [1, 52, 254],
    [0, 56, 252],
    [2, 59, 255],
    [0, 65, 252],
    [1, 68, 255],
    [0, 73, 253],
    [1, 75, 255],
    [0, 82, 253],
    [0, 84, 255],
    [0, 89, 252],
    [1, 92, 255],
    [0, 97, 253],
    [1, 99, 255],
    [0, 105, 253],
    [2, 107, 255],
    [0, 113, 252],
    [1, 116, 255],
    [0, 121, 253],
    [0, 122, 254],
    [0, 128, 252],
    [2, 131, 255],
    [0, 136, 253],
    [2, 138, 255],
    [0, 145, 253],
    [1, 147, 255],
    [0, 152, 252],
    [2, 155, 255],
    [0, 160, 253],
    [0, 162, 255],
    [0, 169, 253],
    [1, 171, 255],
    [0, 176, 252],
    [2, 179, 255],
    [0, 184, 253],
    [2, 186, 255],
    [0, 192, 253],
    [1, 194, 255],
    [0, 199, 253],
    [2, 202, 255],
    [0, 208, 253],
    [1, 211, 255],
    [0, 216, 253],
    [1, 218, 255],
    [0, 226, 255],
    [1, 227, 255],
    [0, 232, 253],
    [1, 235, 255],
    [0, 240, 252],
    [1, 242, 254],
    [0, 248, 254],
    [2, 250, 255],
    [4, 254, 249],
    [5, 255, 250],
    [12, 255, 242],
    [12, 255, 242],
    [20, 254, 234],
    [21, 255, 235],
    [28, 254, 226],
    [28, 254, 226],
    [35, 254, 220],
    [36, 255, 221],
    [44, 254, 211],
    [44, 254, 211],
    [53, 253, 202],
    [54, 254, 203],
    [61, 254, 195],
    [61, 254, 195],
    [68, 254, 185],
    [69, 255, 186],
    [74, 254, 179],
    [75, 255, 180],
    [83, 254, 171],
    [84, 255, 172],
    [91, 254, 161],
    [92, 255, 162],
    [99, 254, 156],
    [100, 255, 157],
    [107, 254, 146],
    [108, 255, 147],
    [117, 253, 138],
    [118, 254, 139],
    [124, 253, 130],
    [125, 254, 131],
    [130, 255, 121],
    [130, 255, 121],
    [136, 255, 115],
    [137, 255, 116],
    [147, 255, 107],
    [147, 255, 107],
    [155, 254, 99],
    [156, 255, 100],
    [162, 254, 94],
    [162, 254, 94],
    [170, 254, 84],
    [171, 255, 85],
    [179, 254, 76],
    [179, 254, 76],
    [186, 254, 68],
    [187, 255, 69],
    [194, 254, 58],
    [195, 255, 59],
    [202, 254, 53],
    [203, 255, 54],
    [210, 254, 44],
    [211, 255, 45],
    [219, 254, 35],
    [220, 255, 36],
    [225, 254, 29],
    [226, 255, 30],
    [234, 254, 20],
    [235, 255, 21],
    [243, 253, 11],
    [244, 254, 12],
    [251, 253, 6],
    [252, 254, 7],
    [255, 250, 1],
    [254, 248, 0],
    [255, 243, 0],
    [253, 241, 0],
    [255, 235, 1],
    [252, 232, 0],
    [255, 226, 1],
    [255, 225, 0],
    [255, 219, 0],
    [253, 217, 0],
    [255, 211, 1],
    [253, 208, 0],
    [255, 202, 3],
    [254, 200, 1],
    [255, 194, 2],
    [253, 192, 0],
    [255, 187, 1],
    [253, 184, 0],
    [254, 180, 0],
    [252, 178, 0],
    [255, 171, 2],
    [253, 169, 0],
    [255, 163, 3],
    [253, 160, 0],
    [255, 155, 2],
    [252, 152, 0],
    [255, 146, 2],
    [254, 144, 0],
    [255, 138, 2],
    [253, 136, 0],
    [255, 131, 2],
    [252, 128, 0],
    [255, 124, 1],
    [253, 121, 0],
    [255, 116, 0],
    [254, 114, 0],
    [255, 107, 0],
    [255, 105, 0],
    [255, 100, 1],
    [254, 97, 0],
    [255, 92, 2],
    [252, 89, 0],
    [254, 84, 2],
    [252, 82, 0],
    [255, 74, 2],
    [254, 72, 0],
    [255, 68, 2],
    [253, 65, 0],
    [255, 60, 0],
    [253, 58, 0],
    [255, 52, 0],
    [253, 50, 0],
    [255, 45, 2],
    [252, 42, 0],
    [255, 36, 2],
    [253, 34, 0],
    [253, 27, 0],
    [252, 26, 0],
    [255, 20, 2],
    [253, 17, 0],
    [255, 12, 2],
    [253, 10, 0],
    [254, 4, 1],
    [252, 2, 0],
    [248, 1, 0],
    [247, 0, 0],
    [241, 0, 0],
    [239, 0, 0],
    [231, 0, 1],
    [230, 0, 0],
    [224, 1, 0],
    [223, 0, 0],
    [215, 0, 0],
    [214, 0, 0],
    [208, 1, 0],
    [207, 0, 0],
    [199, 1, 2],
    [198, 0, 1],
    [192, 0, 1],
    [191, 0, 0],
    [184, 1, 0],
    [183, 0, 0],
    [178, 1, 0],
    [176, 0, 0],
    [168, 1, 0],
    [167, 0, 0],
    [160, 1, 1],
    [159, 0, 0],
    [152, 0, 0],
    [151, 0, 0],
    [144, 1, 1],
    [143, 0, 0],
    [136, 1, 1],
    [134, 0, 0],
    [129, 0, 0],
    [128, 0, 0],
  ],
  autumn: [
    [253, 0, 0],
    [254, 1, 1],
    [254, 1, 0],
    [255, 2, 0],
    [254, 4, 0],
    [254, 4, 0],
    [254, 6, 0],
    [254, 6, 0],
    [254, 8, 1],
    [255, 9, 2],
    [253, 10, 0],
    [254, 11, 1],
    [253, 12, 0],
    [254, 13, 1],
    [254, 13, 0],
    [255, 14, 0],
    [253, 17, 0],
    [253, 17, 0],
    [254, 18, 0],
    [254, 18, 0],
    [254, 20, 0],
    [255, 21, 0],
    [253, 22, 0],
    [254, 23, 1],
    [253, 24, 0],
    [254, 25, 0],
    [253, 27, 0],
    [253, 27, 0],
    [253, 27, 0],
    [254, 28, 0],
    [254, 30, 0],
    [254, 30, 0],
    [254, 32, 0],
    [255, 33, 0],
    [253, 34, 0],
    [253, 34, 0],
    [254, 35, 0],
    [255, 36, 0],
    [255, 38, 0],
    [255, 38, 0],
    [253, 41, 0],
    [253, 41, 0],
    [253, 43, 0],
    [253, 43, 0],
    [254, 44, 0],
    [255, 45, 0],
    [253, 46, 0],
    [254, 47, 1],
    [253, 48, 0],
    [254, 49, 0],
    [255, 50, 0],
    [255, 50, 0],
    [255, 52, 0],
    [255, 52, 0],
    [254, 54, 0],
    [255, 55, 0],
    [254, 55, 1],
    [255, 56, 2],
    [253, 57, 0],
    [253, 57, 0],
    [253, 60, 0],
    [254, 61, 0],
    [255, 62, 0],
    [255, 62, 0],
    [253, 65, 0],
    [253, 65, 0],
    [254, 66, 0],
    [255, 67, 1],
    [254, 68, 0],
    [254, 68, 0],
    [253, 69, 0],
    [254, 70, 1],
    [254, 72, 0],
    [254, 72, 0],
    [254, 75, 0],
    [254, 75, 0],
    [255, 76, 0],
    [255, 76, 0],
    [254, 78, 0],
    [255, 79, 1],
    [254, 80, 0],
    [255, 81, 0],
    [253, 81, 0],
    [254, 82, 1],
    [253, 85, 0],
    [253, 85, 0],
    [254, 86, 0],
    [254, 86, 0],
    [255, 88, 0],
    [255, 88, 0],
    [254, 90, 0],
    [255, 91, 0],
    [255, 93, 0],
    [255, 93, 0],
    [253, 93, 0],
    [254, 94, 1],
    [254, 96, 0],
    [254, 96, 0],
    [254, 98, 1],
    [254, 98, 1],
    [253, 100, 0],
    [254, 101, 1],
    [254, 101, 0],
    [255, 102, 1],
    [255, 103, 0],
    [255, 104, 0],
    [255, 105, 0],
    [255, 106, 0],
    [254, 108, 0],
    [254, 108, 0],
    [254, 110, 1],
    [254, 110, 1],
    [253, 112, 0],
    [254, 113, 1],
    [255, 114, 1],
    [255, 114, 1],
    [254, 115, 0],
    [255, 116, 0],
    [254, 118, 0],
    [254, 118, 0],
    [254, 120, 2],
    [254, 120, 2],
    [254, 122, 1],
    [254, 122, 1],
    [253, 124, 0],
    [254, 125, 1],
    [254, 125, 0],
    [255, 126, 0],
    [252, 128, 0],
    [253, 129, 0],
    [254, 130, 1],
    [254, 130, 1],
    [254, 132, 0],
    [254, 132, 0],
    [254, 134, 1],
    [254, 134, 1],
    [253, 136, 0],
    [254, 137, 1],
    [254, 139, 0],
    [254, 139, 0],
    [254, 139, 0],
    [255, 140, 0],
    [254, 142, 1],
    [254, 142, 1],
    [254, 144, 0],
    [254, 144, 0],
    [254, 146, 1],
    [255, 147, 2],
    [255, 148, 1],
    [255, 148, 1],
    [254, 149, 0],
    [255, 150, 0],
    [252, 152, 0],
    [253, 153, 0],
    [253, 155, 0],
    [253, 155, 0],
    [254, 156, 0],
    [254, 156, 0],
    [254, 158, 1],
    [255, 159, 2],
    [253, 160, 0],
    [254, 161, 1],
    [254, 161, 0],
    [255, 162, 0],
    [255, 164, 0],
    [255, 164, 0],
    [255, 166, 0],
    [255, 166, 0],
    [254, 168, 2],
    [254, 168, 2],
    [254, 170, 1],
    [255, 171, 2],
    [254, 173, 1],
    [254, 173, 1],
    [254, 173, 0],
    [255, 174, 0],
    [254, 177, 0],
    [254, 177, 0],
    [255, 178, 0],
    [255, 179, 1],
    [254, 180, 0],
    [254, 180, 0],
    [254, 182, 1],
    [255, 183, 2],
    [253, 184, 0],
    [254, 185, 1],
    [253, 186, 0],
    [254, 187, 0],
    [255, 188, 0],
    [255, 188, 0],
    [255, 190, 0],
    [255, 190, 0],
    [254, 192, 0],
    [255, 193, 1],
    [254, 193, 1],
    [255, 194, 2],
    [255, 195, 0],
    [255, 196, 1],
    [254, 197, 0],
    [255, 198, 0],
    [254, 201, 0],
    [254, 201, 0],
    [253, 203, 0],
    [253, 203, 0],
    [254, 204, 0],
    [255, 205, 1],
    [254, 205, 1],
    [255, 206, 2],
    [253, 207, 0],
    [254, 208, 1],
    [255, 210, 0],
    [255, 210, 0],
    [255, 212, 0],
    [255, 212, 0],
    [255, 214, 0],
    [255, 214, 0],
    [254, 215, 1],
    [255, 216, 2],
    [255, 218, 2],
    [255, 218, 2],
    [253, 219, 0],
    [254, 220, 1],
    [255, 222, 0],
    [255, 222, 0],
    [254, 224, 0],
    [254, 224, 0],
    [255, 225, 0],
    [255, 225, 0],
    [254, 228, 0],
    [255, 229, 0],
    [254, 229, 0],
    [255, 230, 1],
    [253, 231, 0],
    [254, 232, 1],
    [254, 234, 0],
    [254, 234, 0],
    [255, 236, 0],
    [255, 236, 0],
    [255, 237, 0],
    [255, 238, 1],
    [254, 239, 0],
    [255, 240, 0],
    [255, 242, 1],
    [255, 242, 1],
    [255, 243, 0],
    [255, 244, 1],
    [255, 246, 0],
    [255, 246, 0],
    [254, 248, 0],
    [254, 248, 0],
    [253, 250, 0],
    [254, 251, 0],
    [255, 252, 0],
    [255, 252, 0],
    [254, 253, 0],
    [255, 254, 1],
  ],
};

export default heatmaps;
